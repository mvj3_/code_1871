//题目分析:
//遇到字母可以直接输出。
//遇到数字后，记录下数字前的那个字母，因为那个是需要重复敲出的，
//然后读完所有数字后算出重复个数，再敲出即可。
//具体读取数字方法见代码解释。
//重重的PS，这题目需要注意两个地方，
//1.字符串结束是以数字结束的话怎么办，
//2.如果他给你一个a1b.....怎么办，例如a1b2c3。

//题目网址:http://soj.me/1753

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
    string msg;
    int num = 0;
    bool isNumber = false;
    char previous = NULL;
    while (cin >> msg && msg != "XXX") {
        for (int i = 0; i < msg.length(); i++) {
            if ('0' <= msg[i] && msg[i] <= '9') {
                if (!isNumber) {
                    previous = msg[i-1];
                }
                isNumber = true;
                num = num * 10 + msg[i] - '0';               //算出数字
            } else {
                if (isNumber) {
                    if (num != 1) {
                        cout << setw(num - 1) << setfill(previous) << previous;
                    }
                    num = 0;
                    isNumber = false;
                    previous = NULL;
                }
                cout << msg[i];
            }
        }

        if (isNumber) {
            if (num != 1) {
                cout << setw(num - 1) << setfill(previous) << previous;
            }
            num = 0;
            isNumber = false;
            previous = NULL;
        }

        cout << endl;
    }

    return 0;
}